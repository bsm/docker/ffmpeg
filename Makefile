DOCKER=$(shell which docker || which podman)

BUILDS=$(patsubst %/Dockerfile,build/%,$(sort $(wildcard */Dockerfile)))
PUSHES=$(patsubst %/Dockerfile,push/%,$(sort $(wildcard */Dockerfile)))

build: $(BUILDS)
push: $(PUSHES)

.PHONY: build push

# ---------------------------------------------------------------------

build/%: %
	$(DOCKER) build --force-rm --compress -t blacksquaremedia/ffmpeg:$< $</

push/%: % build/%
	$(DOCKER) push blacksquaremedia/ffmpeg:$<
