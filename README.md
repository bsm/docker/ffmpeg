# ffmpeg

This repository compiles the [FFmpeg](https://ffmpeg.org/) without the GPL/nonfree components and packs it into [blacksquaremedia/ffmpeg](https://hub.docker.com/r/blacksquaremedia/ffmpeg) docker container.

The FFmpeg binary is compiled according to [Compile FFmpeg for Ubuntu, Debian, or Mint](http://trac.ffmpeg.org/wiki/CompilationGuide/Ubuntu) guide.
